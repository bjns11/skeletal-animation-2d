use bevy::prelude::*;
use bevy_inspector_egui::Inspectable;
use serde::{Serialize, Deserialize};

static mut BONE_COUNT: i32 = 0;

#[derive(Component, Clone, Serialize, Deserialize, Debug)]
pub struct Skin {
    pub name: String,
    pub file_name: String,
    pub bone_name: String,
    pub pivot: Vec2,
    pub rotation: f32,
    pub width: usize,
    pub height: usize,
}
impl Skin {
    pub fn new(file_name: String, bone_name: String, width: usize, height: usize) -> Self {
        Skin {
            name: "".to_string(),
            bone_name,
            file_name,
            pivot: Vec2::new(0., 0.),
            width,
            height,
            rotation: 0.,
        }
    }
}

#[derive(Component)]
pub struct Selectable {
    pub is_selected: bool,
}

#[derive(Default, Component, Clone, Debug, Serialize, Deserialize)]
pub struct BoneAnimation {
    pub key_frames: Vec<KeyFrame>,
}

#[derive(Default, Clone, Debug, Serialize, Deserialize)]
pub struct KeyFrame {
    pub translation: Vec3,
    pub scale: Vec3,
    pub rotation: f32,
}

#[derive(Default)]
pub struct Bones(pub Vec<Entity>);

#[derive(Component, Clone, Inspectable, Debug, Serialize, Deserialize)]
pub struct Bone {
    pub name: String,
    pub parent: String,
    pub length: f32,
    pub translation: Vec3,
    pub scale: Vec3,
    pub rotation: f32,
}
impl Bone {
    pub fn new(translation: Vec3) -> Bone {
        Bone {
            name: Bone::new_name(),
            parent: String::new(),
            length: 100.,
            translation,
            scale: Vec3::new(1., 1., 0.),
            rotation: 0.,
        }
    }
    pub fn new_with_parent(translation: Vec3, parent: String, bones: Vec<Bone>) -> Bone {
        let mut rel_translation = translation;
        let mut rel_rotation = 0.;
        for bone in bones.clone() {
            if parent.eq(&bone.name) {
                rel_rotation = -bone.get_gl_rotation(bones);
                rel_translation = Quat::mul_vec3(
                    Quat::from_euler(EulerRot::XYZ, 0., 0., rel_rotation),
                    Vec3::new(translation.x, translation.y, 0.),
                );
                break;
            }
        }
        Bone {
            name: Bone::new_name(),
            length: 100.,
            translation: rel_translation,
            parent,
            scale: Vec3::new(1., 1., 0.),
            rotation: rel_rotation,
        }
    }
    fn new_name() -> String {
        unsafe {
            BONE_COUNT += 1;
            format!("bone_{}", BONE_COUNT - 1)
        }
    }
    pub fn get_gl_translation(&self, bones: Vec<Bone>) -> Vec3 {
        for bone in bones.clone() {
            if self.parent.eq(&bone.name) {
                let rel_translation_rotated = Quat::mul_vec3(
                    Quat::from_euler(EulerRot::XYZ, 0., 0., bone.get_gl_rotation(bones.clone())),
                    Vec3::new(self.translation.x, self.translation.y, 0.),
                );
                return rel_translation_rotated * bone.scale + bone.get_gl_translation(bones.clone());
            }
        }
        self.translation
    }
    pub fn get_gl_rotation(&self, bones: Vec<Bone>) -> f32 {
        for bone in bones.clone() {
            if self.parent.eq(&bone.name) {
                return self.rotation + bone.get_gl_rotation(bones);
            }
        }
        self.rotation
    }
}