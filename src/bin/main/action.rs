use std::f32::consts::PI;

use crate::*;
use bevy::prelude::*;

pub struct EditorAction {
    pub easing_function: String,
    pub show_bones: bool,
    pub is_running: bool,
    pub frames_since_run: usize,
    pub selected_entity: Option<Entity>,
    pub selected_bone_name: String,
    pub skin_file_name: String,
    pub taken: bool,
    pub kind: ActionKind,
    pub mouse_anchor: Vec3,
}
impl Default for EditorAction {
    fn default() -> Self {
        EditorAction {
            easing_function: "linear".to_string(),
            show_bones: true,
            is_running: false,
            frames_since_run: 0,
            selected_entity: None,
            selected_bone_name: String::new(),
            skin_file_name: {
                let paths = fs::read_dir("./assets/img/").unwrap();
                let mut first_name = String::new();
                for path in paths {
                    first_name = path
                        .unwrap()
                        .path()
                        .file_name()
                        .unwrap()
                        .to_str()
                        .unwrap()
                        .to_string();
                    break;
                }
                first_name
            },
            taken: false,
            kind: ActionKind::NONE,
            mouse_anchor: Vec3::ZERO,
        }
    }
}

pub fn move_arm(
    mut q: Query<(&mut Bone, &Selectable)>,
    keys: Res<Input<KeyCode>>,
    windows: Res<Windows>,
) {
    if !keys.pressed(KeyCode::O) {
        return;
    }
    let mut done = false;
    for i in 0..80 {
        if done {
            break;
        }
        let mouse_pos = get_mouse_pos(&windows);
        // let bones = q
        //     .iter()
        //     .map(|(bone, _)| bone.clone())
        //     .collect::<Vec<Bone>>();
        let mut end_effector: Option<String> = None;
        for (bone, selectable) in q.iter_mut() {
            if selectable.is_selected {
                end_effector = Some(bone.name.to_string());
                break;
            }
        }
        let mut bones = q
            .iter()
            .map(|(bone, _)| bone.clone())
            .collect::<Vec<Bone>>();
        match end_effector.clone() {
            Some(name) => {
                let end_effector_name = end_effector.unwrap();
                let mut next_bone: Option<Bone> = None;
                for b in bones.clone() {
                    if b.name.eq(&name) {
                        next_bone = Some(b);
                        break;
                    }
                }
                for j in 0..i/10 + 1 {
                    match next_bone.clone() {
                        Some(current_bone) => {
                            bones = q
                                .iter()
                                .map(|(bone, _)| bone.clone())
                                .collect::<Vec<Bone>>();
                            let tip_bone = bones
                                .clone()
                                .iter()
                                .find(|bone| bone.name.eq(&end_effector_name))
                                .unwrap()
                                .clone();
                            let tip = tip_bone.get_gl_translation(bones.clone())
                                + Quat::from_euler(
                                    EulerRot::XYZ,
                                    0.,
                                    0.,
                                    tip_bone.get_gl_rotation(bones.clone()),
                                )
                                .mul_vec3(Vec3::new(
                                    0.,
                                    tip_bone.length * tip_bone.scale.y,
                                    0.,
                                ));
                            let cur_to_tip = tip - current_bone.get_gl_translation(bones.clone());
                            let cur_to_target =
                                mouse_pos - current_bone.get_gl_translation(bones.clone());
                            let angle_diff = get_2d_angle_standardized(cur_to_tip)
                                - get_2d_angle_standardized(cur_to_target);
                            for (mut bone, _) in q.iter_mut() {
                                if bone.name.eq(&current_bone.name) {
                                    bone.rotation = bone.rotation + angle_diff;
                                }
                            }
                            let mut found = false;
                            for b in bones.clone() {
                                if b.name.eq(&current_bone.parent) {
                                    next_bone = Some(b);
                                    found = true;
                                    break;
                                }
                            }
                            if !found {
                                next_bone = None;
                                done = true;
                            }
                        }
                        None => break,
                    }
                }
            }
            None => return,
        }
    }
}

pub fn transform_bone(
    mut q: Query<(&mut Bone, &Selectable)>,
    mut action: ResMut<EditorAction>,
    windows: Res<Windows>,
) {
    let mouse_pos = get_mouse_pos(&windows);
    let movement = (mouse_pos - action.mouse_anchor) as Vec3;
    let bones = q
        .iter()
        .map(|(bone, _)| bone.clone())
        .collect::<Vec<Bone>>();
    match action.kind {
        ActionKind::NONE => (),
        ActionKind::ROTATE => {
            for (mut bone, selectable) in q.iter_mut() {
                if selectable.is_selected {
                    bone.rotation = bone.rotation - movement.x / 50.;
                }
            }
        }
        ActionKind::SCALE => {
            for (mut bone, selectable) in q.iter_mut() {
                if selectable.is_selected {
                    bone.scale.x += movement.x / 50.;
                    bone.scale.y += movement.x / 50.;
                }
            }
        }
        ActionKind::MOVE => {
            for (mut bone, selectable) in q.iter_mut() {
                if selectable.is_selected {
                    let mut moved = false;
                    for parent in bones.clone() {
                        if parent.name.eq(&bone.parent) {
                            bone.translation += Quat::mul_vec3(
                                Quat::from_euler(
                                    EulerRot::XYZ,
                                    0.,
                                    0.,
                                    -parent.get_gl_rotation(bones.clone()),
                                ),
                                movement,
                            );
                            moved = true;
                            break;
                        }
                    }
                    if moved == false {
                        bone.translation += movement;
                    }
                }
            }
        }
    }
    action.mouse_anchor = mouse_pos;
}

pub fn delete_bone(
    mut commands: Commands,
    keys: Res<Input<KeyCode>>,
    q: Query<(Entity, &Selectable)>,
    children_query: Query<&Children>,
) {
    if keys.just_pressed(KeyCode::Delete) {
        for (entity, selectable) in q.iter() {
            if selectable.is_selected {
                for children in children_query.iter() {
                    for child in children.to_vec() {
                        let mut remove_entity = || -> Result<(), Error> {
                            commands.entity(child).despawn();
                            Ok(())
                        };
                        if let Err(_err) = remove_entity() {
                            println!("Entity to be removed doesn't exist!");
                        }
                    }
                }

                let mut remove_entity = || -> Result<(), Error> {
                    commands.entity(entity).despawn();
                    Ok(())
                };
                if let Err(_err) = remove_entity() {
                    println!("Entity to be removed doesn't exist!");
                }
            }
        }
    }
}

pub fn add_bone(
    asset_server: Res<AssetServer>,
    keys: Res<Input<KeyCode>>,
    mouse: Res<Input<MouseButton>>,
    mut commands: Commands,
    windows: Res<Windows>,
    mut action: ResMut<EditorAction>,
    mut q: Query<(&Bone, &mut Selectable)>,
) {
    if !keys.pressed(KeyCode::LControl) {
        return;
    }
    if action.taken {
        return;
    } else {
        action.taken = true;
    }
    let mut mouse_pos = Vec2::new(0., 0.);
    let window = windows.get_primary().unwrap();
    if let Some(_position) = window.cursor_position() {
        mouse_pos = _position;
        mouse_pos.x -= window.width() / 2.;
        mouse_pos.y -= window.height() / 2.;
    }

    if mouse.just_pressed(MouseButton::Left) {
        let mut parent = None;
        for (bone, selectable) in q.iter() {
            if selectable.is_selected {
                parent = Some(bone.clone());
            }
        }
        // unselect all except new bone
        for (_, mut selectable) in q.iter_mut() {
            selectable.is_selected = false;
        }
        let bones = q
            .iter()
            .map(|(bone, _)| bone.clone())
            .collect::<Vec<Bone>>();
        let length = 100.;
        let new_bone = match parent {
            None => Bone::new(Vec3::new(mouse_pos.x, mouse_pos.y, 0.)),
            Some(parent) => Bone::new_with_parent(
                Vec3::new(
                    mouse_pos.x - parent.get_gl_translation(bones.clone()).x,
                    mouse_pos.y - parent.get_gl_translation(bones.clone()).y,
                    0.,
                ) / parent.scale,
                parent.name,
                bones,
            ),
        };
        let new_bone_name = new_bone.name.clone();
        let parent_entity = commands
            .spawn_bundle(GeometryBuilder::build_as(
                &shapes::Polygon {
                    points: vec![
                        Vec2::new(0., 0.),
                        Vec2::new(-0.1, 0.1),
                        Vec2::new(0., 1.),
                        Vec2::new(0.1, 0.1),
                        Vec2::new(0., 0.),
                    ],
                    closed: true,
                },
                DrawMode::Stroke {
                    0: StrokeMode::new(COLOR_DEFAULT, 2. / length),
                },
                Transform {
                    translation: Vec3::new(mouse_pos.x, mouse_pos.y, -10.),
                    scale: Vec3::new(length, length, length),
                    ..Default::default()
                },
            ))
            .insert(new_bone)
            .insert(Selectable { is_selected: true })
            .insert(BoneAnimation::default())
            .id();
        let child_entity = commands
            .spawn()
            .insert(Transform {
                translation: Vec3::new(0., 0.5, 1.),
                ..Default::default()
            })
            .insert(Parent(parent_entity))
            .id();
        select_bone(&mut action, child_entity, &new_bone_name)
    }
}

pub fn select_bone(mut action: &mut ResMut<EditorAction>, entity: Entity, bone_name: &str) {
    action.selected_entity = Some(entity);
    action.selected_bone_name = bone_name.to_string();
}

pub fn select_all(
    mut action: ResMut<EditorAction>,
    mut q: Query<(&mut Selectable, Entity, &Bone)>,
    keys: Res<Input<KeyCode>>,
) {
    if keys.just_pressed(KeyCode::A) {
        for (mut selectable, entity, bone) in q.iter_mut() {
            selectable.is_selected = true;
            select_bone(&mut action, entity, &bone.name);
        }
    }
}

pub fn select(
    mut action: ResMut<EditorAction>,
    mouse: Res<Input<MouseButton>>,
    windows: Res<Windows>,
    keys: Res<Input<KeyCode>>,
    mut q: Query<(&Transform, &mut Selectable, Entity, &Bone)>,
) {
    if mouse.just_pressed(MouseButton::Left) {
        if action.taken {
            return;
        }
        action.taken = true;
        let mut shortest_distance = 5000.;
        if action.kind == ActionKind::NONE {
            let mut closest_entity = None;
            let bones = q
                .iter()
                .map(|(_, _, _, bone)| bone.clone())
                .collect::<Vec<Bone>>();
            for (transform, _, entity, bone) in q.iter_mut() {
                let mouse_pos = get_mouse_pos(&windows);

                // calculate bone center
                let bone_translate = transform.translation;
                let bone_rotation = bone.get_gl_rotation(bones.clone());
                let bone_center = bone_translate
                    + Quat::mul_vec3(
                        Quat::from_euler(EulerRot::XYZ, 0., 0., bone_rotation),
                        Vec3::new(0., bone.length / 3., 0.),
                    );

                let distance = bone_center.truncate().distance(mouse_pos.truncate());
                if distance < shortest_distance && distance < bone.length * bone.scale.y / 2. {
                    // select bone
                    shortest_distance = distance;
                    closest_entity = Some(entity);
                }
            }
            if !keys.pressed(KeyCode::LShift) {
                for (_, mut selectable, _, _) in q.iter_mut() {
                    selectable.is_selected = false;
                }
            }
            match closest_entity {
                Some(entity) => {
                    let mut closest = q.get_mut(entity).unwrap();
                    closest.1.is_selected = !closest.1.is_selected;
                    select_bone(&mut action, closest.2, &closest.3.name);
                }
                None => (),
            }
        }
    }
}

pub fn start_editor_action(
    keys: Res<Input<KeyCode>>,
    mut current_action: ResMut<EditorAction>,
    q: Query<&Selectable>,
) {
    let mut no_selection = true;
    for selectable in q.iter() {
        if selectable.is_selected {
            no_selection = false;
            break;
        }
    }
    if no_selection {
        return;
    }
    if keys.just_pressed(KeyCode::R) {
        current_action.kind = ActionKind::ROTATE;
    }
    if keys.just_pressed(KeyCode::G) {
        current_action.kind = ActionKind::MOVE;
    }
    if keys.just_pressed(KeyCode::S) {
        current_action.kind = ActionKind::SCALE;
    }
}

pub fn end_editor_action(mouse: Res<Input<MouseButton>>, mut action: ResMut<EditorAction>) {
    action.taken = false;
    if mouse.just_pressed(MouseButton::Left) {
        if action.kind != ActionKind::NONE {
            action.taken = true;
            action.kind = ActionKind::NONE;
        }
    }
}

pub fn show_hide_bones(
    mut action: ResMut<EditorAction>,
    keys: Res<Input<KeyCode>>,
    mut q: Query<&mut Visibility, With<Bone>>,
) {
    if keys.just_pressed(KeyCode::D) {
        action.show_bones = !action.show_bones;
    }
    for mut visibility in q.iter_mut() {
        visibility.is_visible = action.show_bones;
    }
}

pub fn add_keyframe(keys: Res<Input<KeyCode>>, mut q: Query<(&mut Bone, &mut BoneAnimation)>) {
    if keys.just_pressed(KeyCode::K) {
        for (bone, mut animation) in q.iter_mut() {
            animation.key_frames.push(KeyFrame {
                translation: bone.translation,
                rotation: bone.rotation,
                scale: bone.scale,
            })
        }
    }
}

pub fn run_animation(
    mut action: ResMut<EditorAction>,
    keys: Res<Input<KeyCode>>,
    mut q: Query<(&mut Bone, &mut BoneAnimation)>,
) {
    if keys.just_pressed(KeyCode::P) {
        action.is_running = !action.is_running;
        action.frames_since_run = 0;
        if !action.is_running {
            for (mut bone, animation) in q.iter_mut() {
                let key_frame_count = animation.key_frames.len();
                if key_frame_count < 1 {
                    continue;
                }
                let first_key_frame = animation.key_frames.get(0 as usize).unwrap();
                bone.translation = first_key_frame.translation;
                bone.rotation = first_key_frame.rotation;
                bone.scale = first_key_frame.scale;
            }
        }
    }
    if action.is_running {
        let key_frame_length = 30;
        action.frames_since_run += 1;
        for (mut bone, animation) in q.iter_mut() {
            let key_frame_count = animation.key_frames.len();
            if key_frame_count <= 1 {
                continue;
            }
            let current_frame = action.frames_since_run;
            let progress = (current_frame % key_frame_length) as f32 / key_frame_length as f32;
            let x = if action.easing_function.eq("ease_in_out") {
                interpolate::ease_in_out(progress)
            } else if action.easing_function.eq("ease_out_elastic") {
                interpolate::ease_out_elastic(progress)
            } else {
                progress
            };
            let key_frame_a = animation
                .key_frames
                .get((current_frame / key_frame_length) % key_frame_count)
                .unwrap();
            let key_frame_b = animation
                .key_frames
                .get((current_frame / key_frame_length + 1) % key_frame_count)
                .unwrap();
            bone.translation =
                interpolate::lerp(key_frame_a.translation, key_frame_b.translation, x);
            bone.rotation = interpolate::lerp(key_frame_a.rotation, key_frame_b.rotation, x);
            bone.scale = interpolate::lerp(key_frame_a.scale, key_frame_b.scale, x);
        }
    }
}

fn get_2d_angle_standardized(vector: Vec3) -> f32 {
    let angle = vector.angle_between(Vec3::new(0., 1., 0.));
    if vector.x > 0. {
        return angle;
    } else {
        return 2. * PI - angle;
    }
}
