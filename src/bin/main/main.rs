mod components;
mod action;
mod interpolate;

use action::*;
use components::*;
use bevy::input::mouse::*;
use bevy::prelude::*;
use bevy_egui::{egui, EguiContext, EguiPlugin};
use bevy_inspector_egui::{
    Inspectable, InspectorPlugin, RegisterInspectable, WorldInspectorPlugin,
};
use bevy_prototype_lyon::prelude::*;
use core::ops::*;
use imagesize::size;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use std::io::Write;
use std::{fs, io::Error};

const COLOR_SELECTED: Color = Color::rgb(1., 1., 1.);
const COLOR_DEFAULT: Color = Color::rgb(1., 0.6, 0.);

struct UpdateSkinEvent;

#[derive(PartialEq, Eq, Inspectable)]
pub enum ActionKind {
    NONE,
    ROTATE,
    SCALE,
    MOVE,
}

fn ui_action(
    mut egui_context: ResMut<EguiContext>,
    mut action: ResMut<EditorAction>,
    mut update_skin_evw: EventWriter<UpdateSkinEvent>,
) {
    let response = egui::Window::new("Bone Settings")
        .show(egui_context.ctx_mut(), |ui| {
            ui.label("Bone Settings");
            egui::ComboBox::from_label("Easing Function")
                .selected_text(&action.easing_function)
                .show_ui(ui, |ui| {
                    ui.selectable_value(
                        &mut action.easing_function,
                        "linear".to_string(),
                        "linear",
                    );
                    ui.selectable_value(
                        &mut action.easing_function,
                        "ease_in_out".to_string(),
                        "ease_in_out",
                    );
                    ui.selectable_value(
                        &mut action.easing_function,
                        "ease_out_elastic".to_string(),
                        "ease_out_elastic",
                    );
                });
            let choose_skin = egui::ComboBox::from_label("Skin")
                .selected_text(&action.skin_file_name)
                .show_ui(ui, |ui| {
                    let paths = fs::read_dir("./assets/img/").unwrap();
                    for path in paths {
                        let filename = path
                            .unwrap()
                            .path()
                            .file_name()
                            .unwrap()
                            .to_str()
                            .unwrap()
                            .to_string();
                        let option = ui
                            .selectable_value(
                                &mut action.skin_file_name,
                                filename.clone(),
                                filename,
                            );
                        if option.clicked() {
                            update_skin_evw.send(UpdateSkinEvent);
                        }
                        if option.hovered() {
                            action.taken = true;
                        }
                    }
                });
                if choose_skin.response.hovered() {
                    action.taken = true;
                }
        })
        .unwrap()
        .response;
    if response.hovered() {
        action.taken = true;
    }
}

fn update_skin(
    action: Res<EditorAction>,
    mut update_skin_evr: EventReader<UpdateSkinEvent>,
    q: Query<(Entity, &Children, &Bone)>,
    asset_server: Res<AssetServer>,
    mut commands: Commands,
) {
    for _ in update_skin_evr.iter() {
        // let bones = q.iter().collect::<Vec<&Bone>>();
        for (entity, children, bone) in q.iter() {
            if bone.name.eq(&action.selected_bone_name) {
                let child = children.to_vec()[0];
                commands.entity(child).despawn();
                commands.entity(entity).remove::<Children>();
                let image_path = format!("assets/img/{}", &action.skin_file_name);
                let (width, height) = match size(image_path) {
                    Ok(dim) => (dim.width, dim.height),
                    Err(why) => {
                        println!("Error getting dimensions: {:?}", why);
                        (0, 0)
                    }
                };
                let path = format!("img/{}", &action.skin_file_name);
                commands
                    .spawn_bundle(SpriteBundle {
                        texture: asset_server.load(&path),
                        sprite: Sprite {
                            color: Color::rgb(1., 1., 1.),
                            custom_size: Some(Vec2::new(width as f32 / height as f32, 1.)),
                            ..Default::default()
                        },
                        transform: Transform {
                            translation: Vec3::new(0., 0.5, -99.),
                            ..Default::default()
                        },
                        ..Default::default()
                    })
                    .insert(Skin::new(
                        action.skin_file_name.clone(),
                        bone.name.clone(),
                        width,
                        height,
                    ))
                    .insert(Parent(entity));
            }
        }
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct AnimationJson {
    bones: Vec<Bone>,
    animations: Vec<BoneAnimation>,
    skins: Vec<Skin>,
}

fn main() {
    App::new()
        .insert_resource(WindowDescriptor {
            width: 500.,
            height: 500.,
            ..Default::default()
        })
        .insert_resource(EditorAction::default())
        .insert_resource(Bones::default())
        .add_event::<UpdateSkinEvent>()
        .add_plugins(DefaultPlugins)
        // // add inspectors
        // .add_plugin(WorldInspectorPlugin::new())
        // .add_plugin(InspectorPlugin::<EditorAction>::new())
        // .register_inspectable::<Bone>()
        // bevy_egui
        .add_plugin(EguiPlugin)
        .add_system(end_editor_action.label("end_action").before("ui_action"))
        .add_system(ui_action.label("ui_action").before("select"))
        .add_system(update_skin)
        .add_system(show_hide_bones)
        .add_system(save)
        .add_system(load)
        .add_system(select_all)
        .add_system(delete_bone)
        .add_system(run_animation)
        .add_system(add_keyframe)
        .add_system(move_arm)
        .add_plugin(ShapePlugin)
        .add_startup_system(setup_camera)
        .add_system_set(
            SystemSet::new()
                .label("bone_editing")
                .with_system(transform_bone)
                .with_system(update_debug_lines)
                .with_system(add_bone.before("select").after("ui_action"))
                .with_system(start_editor_action.after("ui_action"))
                .with_system(select.label("select").after("ui_action"))
                .with_system(highlight_selection),
        )
        .run();
}

// fn set_skin(mut skins: ResMut<Skins>, asset_server: Res<AssetServer>, file_name: &str) {
//     let image: Handle<Image> = asset_server.load(&file_name);
//     for skin in skins.0 {
//         if skin.file_name.eq(file_name) {
//             // update skin
//             return;
//         }
//     }
//     // push new skin
//     skin.0.push(Skin {
//         name: "skin",
//         file_name: file_name.to_string(),
//         pivot: Vec2::new(0.,0.),
//         rotation: 0,
//         width: image
//     });
// }

fn setup_camera(mut commands: Commands) {
    commands.spawn_bundle(OrthographicCameraBundle::new_2d());
    commands.spawn_bundle(UiCameraBundle::default());
}

fn update_debug_lines(mut q: Query<(&mut Transform, &Bone)>) {
    let bones= q.iter().map(|(_,bone)| bone.clone()).collect::<Vec<Bone>>();
    for (mut transform, bone) in q.iter_mut() {
        let gl_translation = bone.get_gl_translation(bones.clone());
        let gl_rotation = bone.get_gl_rotation(bones.clone());
        transform.translation = Vec3::new(gl_translation.x, gl_translation.y, 100.);
        transform.rotation = Quat::from_euler(EulerRot::XYZ, 0., 0., gl_rotation);
        transform.scale = Vec3::new(bone.length * bone.scale.x, bone.length * bone.scale.y, 0.);
    }
}

fn highlight_selection(mut q: Query<(&mut DrawMode, &Selectable, &Transform), With<Bone>>) {
    for (mut draw_mode, selectable, transform) in q.iter_mut() {
        if let DrawMode::Stroke(ref mut mode) = *draw_mode {
            mode.options.line_width = 2. / transform.scale.x;
            if selectable.is_selected {
                mode.color = COLOR_SELECTED;
            } else {
                mode.color = COLOR_DEFAULT;
            }
        }
    }
}

fn get_mouse_pos(windows: &Res<Windows>) -> Vec3 {
    let mut mouse_pos = Vec3::ZERO;
    let window = windows.get_primary().unwrap();
    if let Some(_position) = window.cursor_position() {
        mouse_pos.x = _position.x - window.width() / 2.;
        mouse_pos.y = _position.y - window.height() / 2.;
    }
    mouse_pos
}

fn save(q: Query<(&Bone, &BoneAnimation)>, keys: Res<Input<KeyCode>>, skin_query: Query<&Skin>) {
    if keys.pressed(KeyCode::LControl) {
        let save_slot = get_just_pressed_number(keys);
        if save_slot == -1 {
            return;
        }
        let bones = q
            .iter()
            .map(|(bone, _)| bone.clone())
            .collect::<Vec<Bone>>();
        let animations = q
            .iter()
            .map(|(_, animation)| animation.clone())
            .collect::<Vec<BoneAnimation>>();
        let skins = skin_query
            .iter()
            .map(|skin| skin.clone())
            .collect::<Vec<Skin>>();
        let serialized = serde_json::to_string(&AnimationJson {
            bones,
            animations,
            skins,
        })
        .unwrap();
        let mut file = fs::File::create(format!("anims/animation_{}.json", save_slot)).unwrap();
        file.write_all(serialized.as_bytes()).unwrap();
        // dbg!(&serialized);
    }
}

fn load(
    asset_server: Res<AssetServer>,
    keys: Res<Input<KeyCode>>,
    mut q: ParamSet<(
        Query<Entity, With<Bone>>, 
        Query<Entity, With<Sprite>>,
    )>,
    mut commands: Commands,
) {
    if keys.pressed(KeyCode::LAlt) {
        let save_slot = get_just_pressed_number(keys);
        if save_slot == -1 {
            return;
        }
        for entity in q.p0().iter() {
            commands.entity(entity).despawn();
        }
        for entity in q.p1().iter() {
            commands.entity(entity).despawn();
        }
        let json = fs::read_to_string(format!("anims/animation_{}.json", save_slot)).unwrap();
        let data: AnimationJson = serde_json::from_str(&json).unwrap();
        for i in 0..data.bones.len() {
            let bone = &data.bones[i];
            let animation = &data.animations[i];
            let mut skin = None;
            for sk in &data.skins {
                if sk.bone_name.eq(&bone.name) {
                    skin = Some(sk);
                    break;
                }
            }
            let bone_entity = commands
                .spawn_bundle(GeometryBuilder::build_as(
                    &shapes::Polygon {
                        points: vec![
                            Vec2::new(0., 0.),
                            Vec2::new(-0.1, 0.1),
                            Vec2::new(0., 1.),
                            Vec2::new(0.1, 0.1),
                            Vec2::new(0., 0.),
                        ],
                        closed: true,
                    },
                    DrawMode::Stroke {
                        0: StrokeMode::new(COLOR_DEFAULT, 2. / bone.length),
                    },
                    Transform {
                        scale: Vec3::new(bone.length, bone.length, 0.),
                        ..Default::default()
                    },
                ))
                .insert(bone.clone())
                .insert(Selectable { is_selected: true })
                .insert(animation.clone())
                .id();
            match skin {
                Some(skin) => {
                    commands
                        .spawn_bundle(SpriteBundle {
                            texture: asset_server.load(&format!("img/{}", &skin.file_name)),
                            sprite: Sprite {
                                color: Color::rgb(1., 1., 1.),
                                custom_size: Some(Vec2::new(
                                    skin.width as f32 / skin.height as f32,
                                    1.,
                                )),
                                ..Default::default()
                            },
                            transform: Transform {
                                translation: Vec3::new(0., 0.5, 0.),
                                ..Default::default()
                            },
                            ..Default::default()
                        })
                        .insert(Parent(bone_entity));
                }
                None => (),
            }
        }
    }
}

fn get_just_pressed_number(keys: Res<Input<KeyCode>>) -> i32 {
    if keys.just_pressed(KeyCode::Key1) {
        return 1;
    } else 
    if keys.just_pressed(KeyCode::Key2) {
        return 2;
    } else 
    if keys.just_pressed(KeyCode::Key3) {
        return 3;
    } else 
    if keys.just_pressed(KeyCode::Key4) {
        return 4;
    } else 
    if keys.just_pressed(KeyCode::Key5) {
        return 5;
    } else 
    if keys.just_pressed(KeyCode::Key6) {
        return 6;
    } else 
    if keys.just_pressed(KeyCode::Key7) {
        return 7;
    } else 
    if keys.just_pressed(KeyCode::Key8) {
        return 8;
    } else 
    if keys.just_pressed(KeyCode::Key9) {
        return 9;
    } else 
    if keys.just_pressed(KeyCode::Key0) {
        return 0;
    } else {
        return -1;
    }
}
