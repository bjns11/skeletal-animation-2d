use bevy::prelude::*;
use bevy_inspector_egui::{WorldInspectorPlugin, Inspectable, RegisterInspectable, InspectorPlugin};

#[derive(Inspectable, Component)]
struct InspectableType;

#[derive(Reflect, Component, Default)]
#[reflect(Component)]
struct ReflectedType;

#[derive(Inspectable, Default)]
struct Data {
  should_render: bool,
  text: String,
  #[inspectable(min = 42.0, max = 100.0)]
  size: f32,
}

fn main() {
  App::new()
    .insert_resource(WindowDescriptor {
      width: 500.,
      height: 500.,
      ..Default::default()
    })
    .add_plugins(DefaultPlugins)
    .add_plugin(WorldInspectorPlugin::new())
    .add_plugin(InspectorPlugin::<Data>::new())
    .add_startup_system(setup)
    .register_inspectable::<InspectableType>() // tells bevy-inspector-egui how to display the struct in the world inspector
    .register_type::<ReflectedType>() // registers the type in the `bevy_reflect` machinery, so that even without implementing `Inspectable` we can display the struct fields
    .run();
}

fn setup(mut commands: Commands) {
  commands.spawn_bundle(OrthographicCameraBundle::new_2d());
}